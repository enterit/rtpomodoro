import { timeWithinDays } from '../src/times'

describe('timeWithinDays', () => {

  test('timeWithinDays', () => {
    const millis = (time) => new Date(time).getTime()
    const testWithinDays = (time, startDay, endDay, expected) =>
      expect(timeWithinDays(millis(time), millis(startDay), millis(endDay))).toBe(expected)
    const isWithinDays    = (time, startDay, endDay) => testWithinDays(time, startDay, endDay, true)
    const isNotWithinDays = (time, startDay, endDay) => testWithinDays(time, startDay, endDay, false)

    isWithinDays   ('2000-01-01 00:00:00', '2000-01-01 00:00:00', '2000-01-01 00:00:00')
    isWithinDays   ('2000-01-01 23:59:59', '2000-01-01 00:00:00', '2000-01-01 00:00:00')
    isNotWithinDays('2000-01-02 00:00:00', '2000-01-01 00:00:00', '2000-01-01 00:00:00')
    isNotWithinDays('2000-01-02 00:00:00', '2000-01-01 00:00:00', '2000-01-01 23:59:59')

    isWithinDays   ('2000-01-01 00:00:00', '2000-01-01 00:00:00', '2000-01-02 00:00:00')
    isWithinDays   ('2000-01-02 00:00:00', '2000-01-01 00:00:00', '2000-01-02 00:00:00')
    isWithinDays   ('2000-01-02 23:59:59', '2000-01-01 00:00:00', '2000-01-02 00:00:00')
    isNotWithinDays('2000-01-03 00:00:00', '2000-01-01 00:00:00', '2000-01-02 00:00:00')
  })

})
