import { updateTasks } from '../src/reducers'

describe('update tasks', () => {
  test('empty tasks, empty update', () => {
    expect(updateTasks([], [])).toEqual([])
  })

  test('fail on tasks without id', () => {
    const task = { name: 'a name' }
    expect(() => updateTasks([task], [])).toThrowError('Task has no id: {"name":"a name"}')
    expect(() => updateTasks([], [task])).toThrowError('Task has no id: {"name":"a name"}')
  })

  test('empty tasks, new task added', () => {
    const newTask = { id: 0, name: 'new task' }
    expect(updateTasks([], [newTask])).toEqual([newTask])
  })

  test('single task update', () => {
    const oldTask = { id: 0, modifiedTime: 0, name: 'old task', dueDate: 0, priority: 1 }
    const newTask = { id: 0, modifiedTime: 1, name: 'new task', dueDate: 1, priority: 2 }
    expect(updateTasks([oldTask], [newTask])).toEqual([newTask])
  })

  test('update one of two tasks', () => {
    const oldTask1 = { id: 1, modifiedTime: 0, name: 'old task 1' }
    const oldTask2 = { id: 2, modifiedTime: 0, name: 'old task 2' }
    const newTask1 = { id: 1, modifiedTime: 1, name: 'new task 1' }
    expect(updateTasks([oldTask1, oldTask2], [newTask1])).toEqual([newTask1, oldTask2])
  })

  test('update one of two tasks - preserve order', () => {
    const oldTask1 = { id: 1, modifiedTime: 0, name: 'old task 1' }
    const oldTask0 = { id: 0, modifiedTime: 0, name: 'old task 0' }
    const newTask1 = { id: 1, modifiedTime: 1, name: 'new task 1' }
    expect(updateTasks([oldTask1, oldTask0], [newTask1])).toEqual([newTask1, oldTask0])
  })

  test('task modified locally later than on server', () => {
    const localTask1 =  { id: 1, modifiedTime: 1, name: 'local task 1' }
    const remoteTask1 = { id: 1, modifiedTime: 0, name: 'remote task 1' }
    expect(updateTasks([localTask1], [remoteTask1])).toEqual([localTask1])
  })

  test('task modified remotely, pomodoros modified locally', () => {
    const currentTask = {
      id: 0, name: 'old task',
      modifiedTime: 0,
      pomodoroModifiedTime: 1,
      pomodoros: { completed: 2, total: 3 },
      savedPomodoros: { completed: 1, total: 2 },
    }
    const receivedTask = {
      id: 0, name: 'new task',
      modifiedTime: 1,
      tags: ['a tag'],
      url: 'a.url',
      pomodoroModifiedTime: 0,
      pomodoros: { completed: 4, total: 5 },
    }
    const expectedMerge = {
      id: 0, name: 'new task',
      modifiedTime: 1,
      tags: ['a tag'],
      url: 'a.url',
      pomodoroModifiedTime: 1,
      pomodoros: { completed: 2, total: 3 },
      savedPomodoros: { completed: 1, total: 2 },
    }
    expect(updateTasks([currentTask], [receivedTask])).toEqual([expectedMerge])
  })

  test('task completed locally, but update from previous action comes with incomplete flag', () => {
    const localTask     = { id: 0, modifiedTime: 0, name: 'old task', savedCompleted: false, completed: true  }
    const remoteTask    = { id: 0, modifiedTime: 1, name: 'new task', savedCompleted: false, completed: false }
    const expectedMerge = { id: 0, modifiedTime: 1, name: 'new task', savedCompleted: false, completed: true  }
    expect(updateTasks([localTask], [remoteTask])).toEqual([expectedMerge])
  })

  test('task uncompleted remotely', () => {
    const localTask     = { id: 0, modifiedTime: 0, name: 'old task', savedCompleted: true, completed: true   }
    const remoteTask    = { id: 0, modifiedTime: 1, name: 'new task', savedCompleted: false, completed: false }
    const expectedMerge = { id: 0, modifiedTime: 1, name: 'new task', savedCompleted: false, completed: false }
    expect(updateTasks([localTask], [remoteTask])).toEqual([expectedMerge])
  })

  test('pomodoros created remotely', () => {
    const localTask  = {
      id: 0, modifiedTime: 1, name: 'a task'
    }
    const remoteTask = {
      id: 0, modifiedTime: 1, name: 'a task',
      pomodoroModifiedTime: 1, pomodoros: { completed: 1, total: 2 }
    }
    const expectedMerge = {
      id: 0, modifiedTime: 1, name: 'a task',
      pomodoroModifiedTime: 1,
      pomodoros: { completed: 1, total: 2 },
      savedPomodoros: { completed: 1, total: 2 }
    }
    expect(updateTasks([localTask], [remoteTask])).toEqual([expectedMerge])
  })

  test('pomodoros removed remotely', () => {
    const localTask = {
      id: 0, modifiedTime: 0, name: 'a task',
      pomodoroModifiedTime: 1,
      pomodoros     : { completed: 1, total: 1 },
      savedPomodoros: { completed: 1, total: 1 },
      providerPomodoroData: { noteId: 0 }
    }
    const remoteTask = {
      id: 0, modifiedTime: 2, name: 'a task',
      pomodoros: { completed: 0, total: 0 }
      // no pomodoroModifiedTime means pomodoro note does not exist
    }
    const expectedMerge = {
      id: 0, modifiedTime: 2, name: 'a task',
      pomodoros     : { completed: 0, total: 0 },
      savedPomodoros: { completed: 0, total: 0 }
    }
    expect(updateTasks([localTask], [remoteTask])).toEqual([expectedMerge])
  })

  test('task completed remotely', () => {
    const localTask     = { id: 0, modifiedTime: 0, name: 'a task' }
    const remoteTask    = { id: 0, modifiedTime: 1, name: 'a task', completed: true }
    const expectedMerge = { id: 0, modifiedTime: 1, name: 'a task', completed: true, savedCompleted: true }
    expect(updateTasks([localTask], [remoteTask])).toEqual([expectedMerge])
  })

  test('task deleted remotely', () => {
    const task  = { id: 0, modifiedTime: 1, name: 'a task' }
    const expectedMerge  = { id: 0, modifiedTime: 1, name: 'a task', deleted: true }
    expect(updateTasks([task], [], [0])).toEqual([expectedMerge])
  })

  test('task properties deleted remotely', () => {
    const localTask      = { id: 0, modifiedTime: 1, name: 'a task', url: 'a.url' }
    const remoteTask     = { id: 0, modifiedTime: 2, name: 'a task' }
    const expectedMerge  = { id: 0, modifiedTime: 2, name: 'a task' }
    expect(updateTasks([localTask], [remoteTask])).toEqual([expectedMerge])
  })

  test('task undeleted remotely', () => {
    const localTask      = { id: 0, modifiedTime: 1, name: 'a task', deleted: true }
    const remoteTask     = { id: 0, modifiedTime: 2, name: 'a task' }
    const expectedMerge  = { id: 0, modifiedTime: 2, name: 'a task' }
    expect(updateTasks([localTask], [remoteTask])).toEqual([expectedMerge])
  })
})
