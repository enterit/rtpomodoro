import { rtmDate } from '../src/rtm'

test('rtmDate', () => {
  const dateString = '2017-05-13T18:23:11Z'
  expect(rtmDate(new Date(dateString).getTime())).toBe(dateString)
})
