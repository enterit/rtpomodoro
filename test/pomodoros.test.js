import { pomodorosFromNote } from '../src/pomodoros'

test('pomodorosFromNote for note title not included in note text', () => {
  const noteText = '[*][*][]'
  expect(pomodorosFromNote(noteText)).toEqual({total: 3, completed: 2})
})

test('pomodorosFromNote for note title included in note text', () => {
  const noteText = 'pomodoro\n[*][*][]'
  expect(pomodorosFromNote(noteText)).toEqual({total: 3, completed: 2})
})
