const fs = require('fs')
const path = require('path')

const devEnv = require('../dev-env.json')
const devEnvLocal = fs.existsSync(path.join(__dirname, '../dev-env.local.json')) ? require('../dev-env.local.json') : {}
module.exports = Object.assign({}, devEnv, devEnvLocal)
