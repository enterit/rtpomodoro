import format from 'string-template'
import { get } from 'lodash'

const getLanguage = () => {
  const language = window.navigator.userLanguage || window.navigator.language
  if (language.includes('ru')) {
    return 'ru'
  } else if (language.includes('ja')) {
    return 'ja'
  } else {
    return 'en'
  }
}

const localizations = {
  en: require('./localization/en.json'),
  ru: require('./localization/ru.json'),
  ja: require('./localization/ja.json')
}

const localize = (key, options) => {
  //const template = get(localizations[getLanguage()], key, `{{${key}}}`)
  const template = get(
    localizations[getLanguage()],
    key,
    get(localizations.en, key, `{{${key}}}`))
  return format(template, options)
}

export default localize
