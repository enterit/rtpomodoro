import { map } from 'lodash'
import md5 from 'md5'
import jsonp from 'jsonp'

// generate unique name for jsonp callback in order to avoid caching
const callbackName = (() => {
  let i = Date.now()
  return () => `callback${i++}`
})()

const rtmDate = (timestamp) => new Date(timestamp).toISOString().replace(/\.\d{3}/, '')

const RTM = () => {
  const apiKey = '734bbe854511bcfaab327e7e45f43a41'
  const secret =  '4c6d985496cc826d'
  const restUrl = document.location.protocol + '//api.rememberthemilk.com/services/rest/'
  let updateQueue =  []
  let authToken =  null
  return {
    setAuthToken(v) { authToken = v },
    saltParams(params) {
      // sort alphabetically
      const sortedKeys = Object.keys(params).sort( (a, b) => a.localeCompare(b) )
      const tokenToSalt = secret + sortedKeys.map( (k) => k + params[k] ).join('')
      //        console.log(tokenToSalt)
      return '' + md5(tokenToSalt)
    },
    paramsToString(params) {
      return map(params, (v, k) => encodeURIComponent(k) + '=' + encodeURIComponent(v) ).join('&')
    },
    getLoginUrl() {
      const params = {api_key: apiKey, perms: 'write'}
      return "http://api.rememberthemilk.com/services/auth/?" + this.paramsToString(params) + "&api_sig=" + this.saltParams(params)
    },
    rest(params) {
      const actualParams = {
        ...params,
        callback: callbackName(),
        format: 'json'
      }
      actualParams.api_sig = this.saltParams(actualParams)
      const url = new URL(restUrl)
      map(actualParams, (v, k) => url.searchParams.append(k, v))
      return new Promise( (resolve, reject) => {
        jsonp(url.toString(), { name: actualParams.callback }, (err, data) => {
          if (err) {
            console.log('error', err, data)
            reject(err)
            return
          }
          if (data && data.rsp && data.rsp.stat === 'ok') {
            resolve(data.rsp)
          } else {
            reject(data)
          }
        })
      })
    },
    restGetAuthToken(frob, callback) {
      const params = {method: 'rtm.auth.getToken', api_key: apiKey, frob: frob}
      this.rest(params).then(callback)
    },
    createGetTasksRequest() {
      // we have to specify timestamp in order to avoid RTM request caching issue
      const filter = '(due:yesterday OR due:today OR due:tomorrow OR tag:pomodoro)'
      return {
        method: 'rtm.tasks.getList', api_key: apiKey, auth_token: authToken,
        filter: filter,
        timestamp: Date.now()
      }
    },
    createGetTaskUpdatesRequest(lastSyncTimestamp) {
      return {
        method: 'rtm.tasks.getList', api_key: apiKey, auth_token: authToken,
        last_sync: rtmDate(lastSyncTimestamp)
      }
    },
    restCreateTimeline() {
      return this.rest({method: 'rtm.timelines.create', api_key: apiKey, auth_token: authToken})
    },
    restTestLogin(callbacks) {
      this.rest({ method: 'rtm.test.login', api_key: apiKey, auth_token: authToken })
        .then(callbacks.success)
        .catch(callbacks.failure)
    },
    createTaskCompleteRequest({ timeline, id, listId, taskSeriesId }) {
      return {
        method: 'rtm.tasks.complete', api_key: apiKey, auth_token: authToken,
        timeline,
        task_id: id,
        list_id: listId,
        taskseries_id: taskSeriesId,
      }
    },
    createNoteCreateRequest({ timeline, title, text, task: { id, listId, taskSeriesId }}) {
      return {
        method: 'rtm.tasks.notes.add', api_key: apiKey, auth_token: authToken,
        timeline: timeline,
        task_id: id,
        list_id: listId,
        taskseries_id: taskSeriesId,
        note_title: title,
        note_text: text
      }
    },
    createNoteUpdateRequest({ timeline, id, title, text }) {
      return {
        method: 'rtm.tasks.notes.edit', api_key: apiKey, auth_token: authToken,
        timeline: timeline,
        note_id: id,
        note_title: title,
        note_text: text
      }
    },
    scheduleCall(func) {
      updateQueue.push(func)
      if (updateQueue.length === 1) {
        this.processUpdateQueue()
      }
    },
    processUpdateQueue() {
      if (updateQueue.length > 0) {
        const updateTask = updateQueue[0]
        updateTask( () => {
          updateQueue = updateQueue.slice(1)
          this.processUpdateQueue()
        })
      }
    },
  }
}

export default RTM
export { rtmDate }
