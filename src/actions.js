import RTM from './rtm'
import RtmTaskProvider from './RtmTaskProvider'
import cookie from 'js-cookie'
import { isEqual, noop, flattenDeep, concat, negate, isNil } from 'lodash'
import { timeWithinDays } from './times'
import { hasPomodoroTag } from './task'
import { pomodorosFromNote } from './pomodoros'

const rtm = RTM()
const taskProvider = RtmTaskProvider(rtm)


const SAVED_POMODOROS = 'SAVED_POMODOROS'
const savedPomodoros = (id, pomodoros, providerPomodoroData, pomodoroModifiedTime) => ({ type: SAVED_POMODOROS, id, pomodoros, providerPomodoroData, pomodoroModifiedTime })

const savePomodoros = (id) => (dispatch, getState) => {
  const getTask = () => getState().tasks.find( (t) => t.id === id )
  const shouldSave = () => ( (t) => t != null && !isEqual(t.pomodoros, t.savedPomodoros) )( getTask() )
  const getPomodoroData = () => ( (t) => (
    t == null ? null : {
      id: t.id,
      pomodoros: t.pomodoros,
      providerPomodoroData: t.providerPomodoroData,
      providerTaskData: t.providerTaskData,
    })
  )( getTask() )
  const afterSave = ({ pomodoros, providerPomodoroData, pomodoroModifiedTime }) => dispatch(savedPomodoros(id, pomodoros, providerPomodoroData, pomodoroModifiedTime))
  taskProvider.savePomodoros({ shouldSave, getPomodoroData, afterSave })
}

const COMPLETE_TASK = 'COMPLETE_TASK'
const COMPLETED_TASK = 'COMPLETED_TASK'
const completeTask = (id) => (dispatch, getState) => {
  dispatch({id, type: COMPLETE_TASK})
  const getTask = () => getState().tasks.find( (t) => t.id === id )
  const getTaskData = () => ( (t) => (
    t == null ? null : {
      id: t.id,
      providerTaskData: t.providerTaskData,
    })
  )( getTask() )
  taskProvider.completeTask({ getTaskData, afterComplete: () => dispatch({id, type: COMPLETED_TASK}) })
}

const ADD_POMODORO = 'ADD_POMODORO'
const addPomodoro = (id) => (dispatch) => {
  dispatch({id, type: ADD_POMODORO})
  dispatch(savePomodoros(id))
}

const REMOVE_POMODORO = 'REMOVE_POMODORO'
const removePomodoro = (id) => (dispatch) => {
  dispatch({id, type: REMOVE_POMODORO})
  dispatch(savePomodoros(id))
}

const COMPLETE_POMODORO = 'COMPLETE_POMODORO'
const completePomodoro = (id) => (dispatch) => {
  dispatch({id, type: COMPLETE_POMODORO})
  dispatch(savePomodoros(id))
}

const UNCOMPLETE_POMODORO = 'UNCOMPLETE_POMODORO'
const uncompletePomodoro = (id) => (dispatch) => {
  dispatch({id, type: UNCOMPLETE_POMODORO})
  dispatch(savePomodoros(id))
}

const START_POMODORO_TIMER = 'START_POMODORO_TIMER'
const startPomodoroTimer = (pomodoroId) => ({
  type: START_POMODORO_TIMER,
  pomodoroId,
  time: Date.now()
})

const START_SHORT_BREAK_TIMER = 'START_SHORT_BREAK_TIMER'

const START_LONG_BREAK_TIMER = 'START_LONG_BREAK_TIMER'
const startLongBreakTimer = () => ({type: START_LONG_BREAK_TIMER, time: Date.now()})

const TIMER_POMODORO = 'TIMER_POMODORO'
const TIMER_LONG_BREAK = 'TIMER_LONG_BREAK'
const TIMER_SHORT_BREAK = 'TIMER_SHORT_BREAK'

const FINISH_TIMER = 'FINISH_TIMER'
const finishTimer = () => (dispatch, getState) => {
  if (getState().timer && getState().timer.type === TIMER_POMODORO) {
    const taskId = getState().timer.pomodoroId
    dispatch({type: FINISH_TIMER})
    dispatch(savePomodoros(taskId))
  } else {
    dispatch({type: FINISH_TIMER})
  }
}

const CANCEL_TIMER = 'CANCEL_TIMER'
const cancelTimer = () => ({type: CANCEL_TIMER})

const ACCEPT_MESSAGE = 'ACCEPT_MESSAGE'
const acceptMessage = () => ({type: ACCEPT_MESSAGE, time: Date.now()})

const CANCEL_MESSAGE = 'CANCEL_MESSAGE'
const cancelMessage = () => ({type: CANCEL_MESSAGE})

const REQUEST_TASKS = 'REQUEST_TASKS'
const RECEIVE_TASKS = 'RECEIVE_TASKS'

const array = (arrayOrObject) => concat(arrayOrObject).filter(negate(isNil))
const eachItem = (arrayOrObject, closure) => array(arrayOrObject).forEach(closure)

const rtmResponseToTasks = (rsp, rtm) => {
  const result = []
  eachItem(rsp.tasks.list, (item) =>
    eachItem(item.taskseries, (serie) =>
      eachItem(serie.task, (serieTask) => {
        /**
         * At some point RTM stopped returning note title as a separate field, and started including it as the first
         * line of the note text. We support both text formats here: '[*][*]...', and 'pomodoro\n[*][*]...'.
         */
        const note = array(serie.notes.note).find( n => n.title === 'pomodoro' || n['$t'].startsWith('pomodoro\n'))
        const noteText = note && note['$t']
        const pomodoros = pomodorosFromNote(noteText)
        const pomodoroNoteId = note && note.id
        const pomodoroModifiedTime = note && note.modified
        const priority = serieTask.priority === 'N' ? undefined: parseInt(serieTask.priority, 10)
        const completed = Boolean(serieTask.completed)
        const timestamp = (dateText) => new Date(dateText).getTime()
        const task = {
          id: serieTask.id,
          name: serie.name,
          completed,
          savedCompleted: completed,
          tags: array(serie.tags.tag),
          url: serie.url,
          priority,
          dueDate: timestamp(serieTask.due),
          modifiedTime: timestamp(serie.modified),
          pomodoros,
          savedPomodoros: pomodoros,
          pomodoroModifiedTime,
          providerPomodoroData: { noteId: pomodoroNoteId },
          providerTaskData: {
            listId: item.id,
            taskSeriesId: serie.id,
            repetitive: serie.rrule != null,
          }
        }
        result.push(task)
      })
    )
  )
  return result
}

const ERR_LOGIN_FAILURE = '98'
const requestTasks = ({ onLoginFailure = noop, onComplete = noop } = {}) => (dispatch) => {
  const lastSync = Date.now()
  dispatch({type: REQUEST_TASKS})
  rtm.setAuthToken(cookie.get('authToken'))
  const currentTime = Date.now()
  rtm.rest(rtm.createGetTasksRequest())
    .then( (rsp) => {
      /**
       * We have to filter tasks by due date, because RTM API does not work correctly with timezones.
       * Tasks with #pomodoro tag are also displayed even if their due date is not today.
       */
      const dueToday = (task) => timeWithinDays(task.dueDate, currentTime, currentTime)
      const taskFilter = (task) => ( dueToday(task) || hasPomodoroTag(task) )
      const tasks = rtmResponseToTasks(rsp).filter(taskFilter)
      dispatch({type: RECEIVE_TASKS, tasks: tasks, lastSync})
      onComplete()
    })
    .catch( (rsp) => {
      console.error(rsp)
      rsp && rsp.rsp && rsp.rsp.err && rsp.rsp.err.code === ERR_LOGIN_FAILURE && onLoginFailure()
    })
}

const RECEIVE_TASK_UPDATES = 'RECEIVE_TASK_UPDATES'
const START_UPDATE_MONITOR = 'START_UPDATE_MONITOR'

const startUpdateMonitor = () => (dispatch, getState) => {
  dispatch({ type: START_UPDATE_MONITOR })
  const update = () => setTimeout(() => {
    rtm.scheduleCall((proceedNext) => {
      const request = rtm.createGetTaskUpdatesRequest(getState().lastSync)
      const lastSync = Date.now()
      const onResponse = (rsp) => {
        const updatedTasks = rtmResponseToTasks(rsp)
        const deletedTaskIds = flattenDeep(
          array(rsp.tasks.list).map( (l) =>
            array(l.deleted).map( (d) =>
              array(d.taskseries.task).map( t => t.id )
            )
          )
        )
        if (updatedTasks.length > 0 || deletedTaskIds.length > 0) {
          dispatch({ type: RECEIVE_TASK_UPDATES, updatedTasks, deletedTaskIds, lastSync })
        }
      }
      rtm
        .rest(request)
        .then(onResponse)
        .finally(() => {
          proceedNext()
          if (getState().monitorUpdates) update()
        })
    })
  }, 2000)
  update()
}

const STOP_UPDATE_MONITOR = 'STOP_UPDATE_MONITOR'

const stopUpdateMonitor = () => ({type: STOP_UPDATE_MONITOR})

export {
  ADD_POMODORO, addPomodoro,
  REMOVE_POMODORO, removePomodoro,
  COMPLETE_POMODORO, completePomodoro,
  UNCOMPLETE_POMODORO, uncompletePomodoro,
  COMPLETE_TASK, COMPLETED_TASK, completeTask,
  SAVED_POMODOROS,
  START_POMODORO_TIMER, startPomodoroTimer,
  START_LONG_BREAK_TIMER, startLongBreakTimer,
  START_SHORT_BREAK_TIMER,
  FINISH_TIMER, finishTimer,
  CANCEL_TIMER, cancelTimer,
  ACCEPT_MESSAGE, acceptMessage,
  CANCEL_MESSAGE, cancelMessage,
  REQUEST_TASKS, requestTasks,
  RECEIVE_TASKS,
  RECEIVE_TASK_UPDATES,
  TIMER_POMODORO,
  TIMER_LONG_BREAK,
  TIMER_SHORT_BREAK,
  START_UPDATE_MONITOR, startUpdateMonitor,
  STOP_UPDATE_MONITOR, stopUpdateMonitor,
}
