const startOfDay = (time) => new Date(time).setHours(0 , 0, 0, 0)
const endOfDay   = (time) => new Date(time).setHours(24, 0, 0, 0)

const timeWithinDays =(time, startDayTime, endDayTime) =>
  time >= startOfDay(startDayTime) && time < endOfDay(endDayTime)

export { timeWithinDays }
