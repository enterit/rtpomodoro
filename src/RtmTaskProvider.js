import { pomodorosToNote } from './pomodoros'

const savePomodoros = (rtm) => ({ shouldSave, getPomodoroData, afterSave }) => {
  if (shouldSave()) {
    rtm.scheduleCall((proceedNext) => {
      if (shouldSave()) {
        rtm.restCreateTimeline().then(({ timeline }) => {
          if (shouldSave()) {
            const { id, pomodoros, providerPomodoroData, providerTaskData } = getPomodoroData()
            let request
            let onResponse
            const noteText = pomodorosToNote(pomodoros, pomodoros)
            const noteModifiedTime = (rsp) => new Date(rsp.note.modified).getTime()
            if (providerPomodoroData.noteId) {
              request = rtm.createNoteUpdateRequest({ timeline, id: providerPomodoroData.noteId, title: 'pomodoro', text: noteText })
              onResponse = (rsp) => afterSave({ pomodoros, providerPomodoroData, pomodoroModifiedTime: noteModifiedTime(rsp) })
            } else {
              request = rtm.createNoteCreateRequest({ timeline, title: 'pomodoro', text: noteText, task: { id, ...providerTaskData } })
              onResponse = (rsp) => afterSave({ pomodoros, providerPomodoroData: { noteId: rsp.note.id }, pomodoroModifiedTime: noteModifiedTime(rsp) })
            }
            rtm
              .rest(request)
              .then(onResponse)
              .finally(proceedNext)
          } else {
            proceedNext()
          }
        })
      } else {
        proceedNext()
      }
    })
  }
}

const completeTask = (rtm) => ({ getTaskData, afterComplete }) => {
  rtm.scheduleCall((proceedNext) => {
    rtm.restCreateTimeline().then(({ timeline }) => {
      const { id, providerTaskData } = getTaskData()
      const request = rtm.createTaskCompleteRequest({ timeline, id, ...providerTaskData })
      rtm
        .rest(request)
        .then(afterComplete)
        .finally(proceedNext)
    })
  })
}

const RtmTaskProvider = (rtm) => {
  return {
    savePomodoros: savePomodoros(rtm),
    completeTask: completeTask(rtm),
  }
}

export default RtmTaskProvider
