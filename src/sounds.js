/*
 * Create sound library. Sound library caches sounds. Also, each sound in the library provides robust play function,
 * which returns a promise that resolves when playback starts or fails
 */
const sounds = ({ volume }) => {
  const cache = {}
  const loadAudio = (path) => {
    if (!cache[path]) {
      cache[path] = new Audio(path)
    }
    return cache[path]
  }

  const load = (path) => {
    const audio = loadAudio(path)
    return {
      play() {
        return new Promise( (resolve, reject) => {
          audio.volume = volume
          audio.play().then(resolve, resolve)
        })
      }
    }
  }

  return {
    load
  }
}

export default sounds
