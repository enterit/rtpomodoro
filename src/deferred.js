const deferred = () => {
  let resolve, reject
  const promise = new Promise((resolveFunc, rejectFunc) => {
    resolve = resolveFunc
    reject = rejectFunc
  })
  return { promise, resolve, reject }
}
export default deferred
