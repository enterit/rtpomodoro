import React from 'react'
import localize from '../localize'

const LongBreak = ({ pomodoroSinceBreak, onBreakClick }) => (
  <div className="center">
    <br/>
    <br/>
    <br/>
    <br/>
    <div className='long-break'>
      <span>{localize('tasks.pomodoroSinceBreak')}</span>
      &nbsp;
      <input type='text' value={pomodoroSinceBreak}/>
      <br/>
      <button onClick={onBreakClick}>
        <span>{localize('tasks.break')}</span>
      </button>
    </div>
  </div>
)

export default LongBreak
