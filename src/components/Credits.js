import React from 'react'
import DocumentTitle from 'react-document-title'
import Navigation from './Navigation'
import localize from '../localize'

const Intro = () => (
  <div className="center">
    <div className="left logo">&nbsp;</div>
    <h1>Credits and Thanks</h1>
  </div>
)

const Thanks = () => (
  <div className="center">
    <br/>
    <br/>
    <div>
      {localize('credits')}
    </div>
    <br/>
    <br/>
  </div>
)

const Credits = () => (
  <div>
    <DocumentTitle title='Credits - Pomodoro for "Remember The Milk"' />
    <section className="top-section">&nbsp;</section>
    <section className="left-section">&nbsp;</section>
    <section className="center-section">
      <Navigation />
      <Intro />
      <Thanks />
    </section>
  </div>
)

export default Credits
