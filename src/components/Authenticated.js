import React from 'react'
import RTM from '../rtm'
import cookie from 'js-cookie'
import { browserHistory } from 'react-router'

class Authenticated extends React.Component {
  componentDidMount() {
    const frobRegex = /frob=(.*)/
    if (frobRegex.test(document.location.href)) {
      // page loaded from scratch
      const frob = document.location.href.match(frobRegex)[1]
      RTM().restGetAuthToken(frob, (rsp) => {
        cookie.set('authToken', rsp.auth.token)
        browserHistory.push('/tasks')
      })
    } else {
      browserHistory.push('/')
    }
  }
  render() { return null }
}

export default Authenticated
