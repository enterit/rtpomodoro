import React from 'react'
import localize from '../localize'
import '../css/tasks.css'

const LoadingSpinner = () => (
  <div className="tasks-loading">
    <span className="spinning">&nbsp;</span>
    &nbsp;
    <span>{localize('tasks.loading')}</span>
  </div>
)

export default LoadingSpinner
