import React from 'react'
import { browserHistory } from 'react-router'
import DocumentTitle from 'react-document-title'
import { createStore, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { connect, Provider } from 'react-redux'
import { isEqual, find } from 'lodash'
import Navigation from './Navigation'
import Header from './Header'
import Task from './Task'
import Timer from './Timer'
import { Message, NotificationBanner } from './Messages'
import LongBreak from './LongBreak'
import LoadingSpinner from './LoadingSpinner'
import Synchronization from './Synchronization'
import { app } from '../reducers'
import localize from '../localize'
import sounds from '../sounds'
import { timeWithinDays } from '../times'
import { hasPomodoroTag } from '../task'
import {
  addPomodoro,
  removePomodoro,
  completePomodoro,
  uncompletePomodoro,
  completeTask,
  startPomodoroTimer,
  startLongBreakTimer,
  finishTimer,
  cancelTimer,
  acceptMessage,
  cancelMessage,
  requestTasks,
  startUpdateMonitor,
  stopUpdateMonitor,
} from '../actions'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
  app,
  composeEnhancers(applyMiddleware(thunkMiddleware))
)
window.store = store

const TaskAdapter = connect(
  null,
  (dispatch, ownProps) => ({
    onAddPomodoro: () => dispatch(addPomodoro(ownProps.id)),
    onRemovePomodoro: () => dispatch(removePomodoro(ownProps.id)),
    onCompletePomodoro: () => dispatch(completePomodoro(ownProps.id)),
    onUncompletePomodoro: () => dispatch(uncompletePomodoro(ownProps.id)),
    onCompleteTask: () => dispatch(completeTask(ownProps.id)),
    onStartTimer: () => dispatch(startPomodoroTimer(ownProps.id)),
  })
)(Task)

const TimerAdapter = connect(
  (state) => {
    const { startTime = 0, duration = 0 } = state.timer ? state.timer: {}
    return { startTime, duration }
  },
  (dispatch) => {
    return {
      onFinish: () => dispatch(finishTimer()),
      onCancel: () => dispatch(cancelTimer())
    }
  }
)(Timer)

const LongBreakAdapter = connect(
  null,
  (dispatch) => ({
    onBreakClick: () => dispatch(startLongBreakTimer())
  })
)(LongBreak)


const soundLoader = new sounds({ volume: 0.2 })
const soundMap = {
  completed: soundLoader.load('/sound/Bell.wav'),
  comeBack : soundLoader.load('/sound/DingLing.wav')
}

const MessageAdapter = connect(
  (state) => ({
    text: state.message ? localize(state.message.key) : null,
    link: '/tasks',
    sound: soundMap[state.message.sound]
  }),
  (dispatch) => ({
    onAccept: () => dispatch(acceptMessage()),
    onCancel: () => dispatch(cancelMessage())
  })
)(Message)

const App = ({ tasks, pomodoroSinceBreak, message, tasksAreLoading, tasksAreSynchronizing }) => (
  <div>
    <DocumentTitle title='Tasks - Pomodoro for "Remember The Milk"' />
    <section className='top-section'>
      { tasksAreSynchronizing && <Synchronization /> }
    </section>
    <section className="left-section">&nbsp;</section>
    <section className="center-section">
      <NotificationBanner />
      { message && <MessageAdapter /> }
      <Navigation />
      <Header />
      <div className="center">
        <div className="tasks">
          { tasksAreLoading && <LoadingSpinner /> }
          {tasks.map( task =>
            <TaskAdapter key={task.id} {...task} />
          )}
          <TimerAdapter />
          <LongBreakAdapter pomodoroSinceBreak={pomodoroSinceBreak}/>
        </div>
      </div>
    </section>
  </div>
)

const maximizeNull = (priority) => priority == null ? Number.MAX_VALUE : priority
const comparePriorities = (a, b) => maximizeNull(a.priority) - maximizeNull(b.priority)
const compareNames = (a, b) => a.name.localeCompare(b.name, undefined, { numeric: true })
const chainComparators = (...comparators) => (a, b) => {
  const comparator = find(comparators, (f) => f(a, b) !== 0)
  return comparator ? comparator(a, b) : 0
}

const sortTasks = (tasks) => tasks.slice().sort(chainComparators(comparePriorities, compareNames))

const AppAdapter = connect(
  (state) => ({
    tasks: sortTasks(
      state.tasks.filter( (t) =>
        !t.completed && !t.deleted &&
        ( hasPomodoroTag(t) || timeWithinDays(t.dueDate, state.startTime, state.lastSync) )
      )
    ),
    pomodoroSinceBreak: state.pomodoroSinceBreak,
    message: state.message,
    tasksAreLoading: state.tasksAreLoading,
    tasksAreSynchronizing: state.tasks.find( (t) => !isEqual(t.pomodoros, t.savedPomodoros) || t.completed !== t.savedCompleted) != null
  })
)(App)

const init = () => {
  const onLoginFailure = () => browserHistory.push('/')
  const onComplete = () => store.dispatch(startUpdateMonitor())
  store.dispatch(requestTasks({ onLoginFailure, onComplete }))
}
const destroy = () => {
  store.dispatch(stopUpdateMonitor())
}

class Wrap extends React.Component {
  componentDidMount() {
    init()
  }
  componentWillUnmount() {
    destroy()
  }
  render() {
    return <Provider store={store}>
      <AppAdapter />
    </Provider>
  }
}

export default Wrap
