import React from 'react'
import localize from '../localize'
import '../css/tasks.css'

function pomodorosToArray(pomodoros){
    if (!pomodoros) return []
    var result = []
    for (var i = 0; i < pomodoros.total; i++) {
        result.push( i < pomodoros.completed ? {class: 'completed'} : {class: 'uncompleted'});
    }
    return result
}
function fixURL(taskURL) {
    return taskURL.indexOf(':') >= 0 ? taskURL : 'http://' + taskURL
}

const getTagsText = (tags) => tags.join(', ')

const Task = ({
  name, tags, pomodoros, url,
  onAddPomodoro,
  onRemovePomodoro,
  onCompletePomodoro,
  onUncompletePomodoro,
  onStartTimer,
  onCompleteTask
}) => (
  <div className='task'>
    <span>
      { tags && tags.length > 0 &&
        <span className='tags'>{getTagsText(tags.sort())}</span>
      }
      <span className='name'>{name}</span>
      { pomodoros &&
        <span className='pomodoros' title={localize('tasks.pomodoroBlockTitle', pomodoros)}>
          { pomodorosToArray(pomodoros).map( (p, i) => <span key={i} className={p.class}>&nbsp;</span> ) }
        </span>
      }
      { url &&
        <span>
          <br/>
          <a href={fixURL(url)}>{url}</a>
        </span>
      }
      <br/>
      <button onClick={onAddPomodoro}        title={localize('tasks.button.addPomodoro')}        >+</button>
      <button onClick={onRemovePomodoro}     title={localize('tasks.button.removePomodoro')}     >-</button>
      <button onClick={onCompletePomodoro}   title={localize('tasks.button.completePomodoro')}   >D</button>
      <button onClick={onUncompletePomodoro} title={localize('tasks.button.uncompletePomodoro')} >U</button>
      <button onClick={onStartTimer}         title={localize('tasks.button.startTimer')}         >~</button>
      &nbsp;&nbsp;&nbsp;&nbsp;
      <button onClick={onCompleteTask}       title={localize('tasks.button.completeTask')}       >C</button>
    </span>
  </div>
)

export default Task
