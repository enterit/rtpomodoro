import React from 'react'
import localize from '../localize'
import '../css/synchronization.css'

const Synchronization = () => (
  <div className="synchronization">
    <span>{localize('tasks.synchronization')}</span>
  </div>
)

export default Synchronization
