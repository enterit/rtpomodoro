import React from 'react'
import '../css/popup.css'
import '../css/timer.css'
import { padStart, noop } from 'lodash'

const Overlay = () => <div className="overlay" />

const timeText = (time) => {
  const timeSec = Math.round(time / 1000)
  const minutes = Math.floor(timeSec / 60)
  const seconds = (timeSec % 60)
  return '' + minutes + ":" + padStart(seconds, 2, '0')
}

class Timer extends React.Component {
  static defaultProps = {
    startTime: 0,
    duration: 0,
    onFinish: noop,
    onCancel: noop
  }
  componentDidMount() {
    this.onUpdate(this.props)
  }
  onUpdate(props) {
    if (this.timeLeft(props) <= 0) return
    this.interval = setInterval(() => {
      if (this.timeLeft(props) <= 0) {
        clearInterval(this.interval)
        this.props.onFinish()
      }
      this.forceUpdate()
    }, 100)
  }
  componentWillReceiveProps(nextProps) {
    clearInterval(this.interval)
    this.onUpdate(nextProps)
  }
  cancel() {
    clearInterval(this.interval)
    this.duration = 0
    this.props.onCancel()
  }
  timeLeft({ startTime, duration }) {
    return startTime + duration - Date.now()
  }
  render() {
    const timeLeft = this.timeLeft(this.props)
    if (timeLeft <= 0) return null
    return <div>
      <Overlay />
      <div className='centered popup timer'>
        <span className='clock-face' id='pomodoro-timer'>{timeText(timeLeft)}</span>
        <ul className='actions'>
          <li><a href="#" onClick={ (e) => { this.cancel(); e.preventDefault(); return false } } className='cancel-pomodoro'>Cancel</a></li>
        </ul>
      </div>
    </div>
  }
}

export default Timer
