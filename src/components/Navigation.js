import React from 'react'
import { Link } from 'react-router'

const Navigation = () => (
  <div className="navigation">
    <div className="navigation">
      <Link to="/">Home</Link>
      &nbsp;
      <Link to="/tasks">Tasks</Link>
      &nbsp;
      <Link to="/credits">Credits</Link>
    </div>
  </div>
)

export default Navigation
