import React from 'react'
import { noop } from 'lodash'
import '../css/popup.css'
import localize from '../localize'
import Push from 'push.js'

class BrowserNotification extends React.Component {
  constructor() {
    super()
    this.onUnload = () => {
      if (this.notification) {
        this.notification.close()
      }
    }
    this.onNotificationClick = noop
    this.onNotificationClose = noop
  }
  createNotification({ text, link, tag }) {
    return Push.create('Recall The Pomodoro', {
      link,
      body: text,
      icon: '/image/notification-icon.png',
      requireInteraction: true,
      onClick: () => {
        try {
          this.onNotificationClick()
        } catch(e) {
          console.log(e)
          // Push notification onClick is is evaluated as string on mobile, so it fails. Ignore. 
        }
      },
      onClose: () => {
        try {
          this.onNotificationClose()
        } catch(e) {
          console.log(e)
          // Push notification onClose is is evaluated as string on mobile, so it fails. Ignore. 
        }
      }
    })
  }
  assignNotificationHandlers(notification, { onClick = noop, onClose = noop, closeOnClick = true , focusOnClick = true }) {
    this.onNotificationClick = () => {
      if (focusOnClick) window.focus()
      if (closeOnClick) {
        notification.close()
      }
      onClick()
    }
    this.onNotificationClose = onClose
  }
  componentDidMount() {
    window.addEventListener('beforeunload', this.onUnload)
    if (this.props.text) {
      this.createNotification(this.props).then((notification) => {
        this.notification = notification
        this.assignNotificationHandlers(this.notification, this.props)
      })
    }
  }
  componentWillUnmount() {
    this.onUnload()
    window.removeEventListener('beforeunload', this.onUnload)
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.text !== nextProps.text) {
      if (this.props.text) {
        this.notification.close()
        this.createNotification(nextProps).then((notification) => {
          this.notification = notification
          this.assignNotificationHandlers(this.notification, nextProps)
        })
      }
    }
    this.assignNotificationHandlers(this.notification, nextProps)
  }
  render() {
    return null
  }
}

class Sound extends React.Component {
  componentDidMount() {
    this.props.sound && this.props.sound.play()
  }
  componentWillReceiveProps(nextProps) {
    nextProps.sound && nextProps.sound.play()
  }
}

const Popup = ({ text, onOk = noop, onCancel = noop }) => {
  if (!text) return null
  return <div>
    <div className="overlay" />
    <div className='centered popup'>
      <div className='text'>{text}</div>
      <ul className='actions'>
        <li><a href='#' onClick={ (e) => { onOk(); e.preventDefault(); return false } }>OK</a></li>
        &nbsp;
        <li><a href='#' onClick={ (e) => { onCancel(); e.preventDefault(); return false } }>Cancel</a></li>
      </ul>
    </div>
  </div>
}

const Message = ({ text, link, sound, onAccept, onCancel }) => {
  return <div>
    <BrowserNotification text={text} onClick={onAccept} onClose={onCancel} link={link}/>
    <Popup text={text} onOk={onAccept} onCancel={onCancel}/>
    <Sound sound={sound}/>
  </div>
}

class NotificationBanner extends React.Component {
  requestPermission() {
    Notification.requestPermission().then(() => this.forceUpdate())
  }
  render() {
    if (Notification.permission !== 'default') return null
    return <section>
      <div>
        <div>
          <span>{localize('tasks.notification.description')}</span>
          &nbsp;
          <button onClick={() => this.requestPermission()}>{localize('tasks.notification.configure')}</button>
        </div>
      </div>
    </section>
  }
}

export { Message, Popup, BrowserNotification, NotificationBanner }
