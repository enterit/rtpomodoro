import React from 'react'
import DocumentTitle from 'react-document-title'
import Navigation from './Navigation'
import localize from '../localize'
import RTM from '../rtm'

const Intro = ({ loginUrl }) => (
  <div className="center">
    <div className="left logo">&nbsp;</div>
    <h1>Recall The Pomodoro</h1>
    <div>
      <span>{localize('main.intro')}</span>
    </div>
    <br/>
    <div>
      <a href={loginUrl}>
        <span>{localize('main.loginText')}</span>
      </a>
    </div>
  </div>
)

const DataUse = () => (
  <div className="center">
    <br/>
    <br/>
    {localize('main.dataUse').split('\n').map( line => <div><span>{line}</span></div> )}
  </div>
)

const Links = () => (
  <div className="center">
    <br/>
    <br/>
    <a href="https://gitlab.com/enterit/rtpomodoro">
      <span>{localize('main.projectPage')}</span>
    </a>
    <br/>
    <a href="mailto:rtpomodoro@devcake.org">
      <span>{localize('main.writeEmail')}</span>
    </a>
  </div>
)

const Main = () => {
  const loginUrl = RTM().getLoginUrl()
  return <div>
    <DocumentTitle title='Pomodoro for "Remember The Milk"' />
    <section className="top-section">&nbsp;</section>
    <section className="left-section">&nbsp;</section>
    <section className="center-section">
      <Navigation />
      <Intro loginUrl={loginUrl}/>
      <DataUse />
      <Links />
    </section>
  </div>
}

export default Main
