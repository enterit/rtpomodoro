import React from 'react'
import Timer from './Timer'

const TestPage = () => (
  <Timer ref={
    (timer) => {
      const go = () => timer.start(
        5,
        () => { console.log('finish'); go() },
        () => { console.log('cancel'); go() }
      )
      go()
    }
  }/>
)
export default TestPage
