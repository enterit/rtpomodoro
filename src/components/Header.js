import React from 'react'
import localize from '../localize'

const Header = () => (
  <div className="left">
    <div className="left logo-small">&nbsp;</div>
    <h1 className="left">
      <span>{localize('tasks.title')}</span>
    </h1>
  </div>
)

export default Header
