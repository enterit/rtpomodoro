import { keyBy, pick, omit, assign, isEqual } from 'lodash'
import {
  ADD_POMODORO,
  REMOVE_POMODORO,
  COMPLETE_POMODORO,
  UNCOMPLETE_POMODORO,
  COMPLETE_TASK, COMPLETED_TASK,
  SAVED_POMODOROS,
  START_POMODORO_TIMER,
  START_LONG_BREAK_TIMER,
  START_SHORT_BREAK_TIMER,
  ACCEPT_MESSAGE,
  CANCEL_MESSAGE,
  FINISH_TIMER,
  CANCEL_TIMER,
  REQUEST_TASKS,
  RECEIVE_TASKS,
  RECEIVE_TASK_UPDATES,
  TIMER_POMODORO,
  TIMER_LONG_BREAK,
  TIMER_SHORT_BREAK,
  START_UPDATE_MONITOR,
  STOP_UPDATE_MONITOR,
} from './actions'

const pomodoroDuration   = 25 * 60 * 1000
const shortBreakDuration = 5 * 60 * 1000
const longBreakDuration  = 15 * 60 * 1000

const initialState = {
  tasks: [],
  //tasks: [
    //{
      //id: 0,
      //name: 'check emails',
      //tags: ['work', 'email'],
      //url: 'google.com',
      //pomodoros     : { completed: 1, total: 3 },
      //savedPomodoros: { completed: 1, total: 3 }
    //},
    //{
      //id: 1,
      //name: 'update the docs',
      //tags: ['work'],
      //pomodoros     : { completed: 2, total: 6 },
      //savedPomodoros: { completed: 2, total: 6 }
    //},
  //],
  pomodoroSinceBreak: 0,
  timer: null,
  message: null,
  tasksAreLoading: false
}

const pomodoro = ({ completed, total }, action) => {
  const actions = {
    ADD_POMODORO() {
      total++
    },
    REMOVE_POMODORO() {
      total = Math.max(0, total - 1)
      completed = Math.min(total, completed)
    },
    COMPLETE_POMODORO() {
      completed++
      total = Math.max(total, completed)
    },
    UNCOMPLETE_POMODORO() {
      completed = Math.max(0, completed - 1)
    }
  }
  if (actions[action.type]) actions[action.type]()
  return { completed, total }
}

const task = (state, action) => {
  switch (action.type) {
    case ADD_POMODORO:
    case REMOVE_POMODORO:
    case COMPLETE_POMODORO:
    case UNCOMPLETE_POMODORO:
      return {
        ...state,
        pomodoros: pomodoro(state.pomodoros, action),
      }
    case COMPLETE_TASK:
      return {
        ...state,
        completed: true
      }
    case COMPLETED_TASK:
      return {
        ...state,
        savedCompleted: true
      }
    case SAVED_POMODOROS:
      return {
        ...state,
        savedPomodoros: action.pomodoros,
        pomodoroModifiedTime: action.pomodoroModifiedTime,
        providerPomodoroData: action.providerPomodoroData
      }
    default:
      return state
  }
}

const mergeTasks = (task, update) => {
  if (task.id === update.id) {
    const checkTaskModifiedTime = (task) => {
      if (task.modifiedTime == null) throw new Error('Task has no modified time: ' + JSON.stringify(task))
    }
    checkTaskModifiedTime(task)
    checkTaskModifiedTime(update)
    const reassign = (task, update, props) => assign(omit(task, props), pick(update, props))
    if (
        task.pomodoroModifiedTime == null ||
        task.pomodoroModifiedTime < update.pomodoroModifiedTime ||
        (
            task.modifiedTime < update.modifiedTime &&
            task.pomodoroModifiedTime < update.modifiedTime &&
            update.pomodoroModifiedTime == null &&
            isEqual(task.pomodoros, task.savedPomodoros)
        )
    ) {
      task = assign(
        reassign(task, update, ['pomodoros', 'pomodoroModifiedTime', 'providerPomodoroData']),
        { savedPomodoros: update.pomodoros },
      )
    }
    if (task.modifiedTime < update.modifiedTime) {
      task = reassign(task, update, ['name', 'modifiedTime', 'tags', 'url', 'deleted', 'dueDate', 'priority'])
      if (task.completed === task.savedCompleted) {
        task = {
          ...task,
          completed: update.completed,
          savedCompleted: update.completed
        }
      }
    }
  }
  return task
}

const updateTasks = (tasks, newTasks, deletedTaskIds = []) => {
  const checkTaskId = (task) => {
    if (task.id == null) throw new Error('Task has no id: ' + JSON.stringify(task))
  }
  tasks.forEach(checkTaskId)
  newTasks.forEach(checkTaskId)
  const newTaskMap = keyBy(newTasks, t => t.id)
  const updatedTasks = tasks
    .map( t => newTaskMap[t.id] ? mergeTasks(t, newTaskMap[t.id]) : t )
    .map( t => deletedTaskIds.includes(t.id) ? { ...t, deleted: true } : t)
  const taskIds = tasks.map( t => t.id )
  const addedTasks = newTasks.filter( t => !taskIds.includes(t.id) )
  return updatedTasks.concat(addedTasks)
}

const app = (state = initialState, action) => {
  switch (action.type) {
    case START_POMODORO_TIMER: {
      return {
        ...state,
        timer: {
          type: TIMER_POMODORO,
          pomodoroId: action.pomodoroId,
          startTime: action.time,
          duration: pomodoroDuration,
        }
      }
    }
    case START_LONG_BREAK_TIMER: {
      return {
        ...state,
        timer: {
          type: TIMER_LONG_BREAK,
          startTime: action.time,
          duration: longBreakDuration
        }
      }
    }
    case FINISH_TIMER:
      if (state.timer) {
        if (state.timer.type === TIMER_POMODORO) {
          const completePomodoro = { type: COMPLETE_POMODORO }
          return {
            ...state,
            tasks: state.tasks.map( t => t.id === state.timer.pomodoroId ? task(t, completePomodoro) : t ),
            pomodoroSinceBreak: state.pomodoroSinceBreak + 1,
            timer: null,
            message: {
              action: START_SHORT_BREAK_TIMER,
              key: 'tasks.message.takeShortBreak',
              sound: 'completed',
            }
          }
        }
        if (state.timer.type === TIMER_LONG_BREAK) {
          return {
            ...state,
            pomodoroSinceBreak: 0,
            timer: null,
            message: {
              action: null,
              key: 'tasks.message.comeBackToWork',
              sound: 'comeBack',
            }
          }
        }
        if (state.timer.type === TIMER_SHORT_BREAK) {
          return {
            ...state,
            timer: null,
            message: {
              action: null,
              key: 'tasks.message.comeBackToWork',
              sound: 'comeBack',
            }
          }
        }
      }
      return state
    case CANCEL_TIMER:
      return {
        ...state,
        timer: null
      }
    case ACCEPT_MESSAGE:
      if (state.message.action === START_SHORT_BREAK_TIMER) {
        return {
          ...state,
          message: null,
          timer: {
            type: TIMER_SHORT_BREAK,
            startTime: action.time,
            duration: shortBreakDuration
          }
        }
      } else {
        return {
          ...state,
          message: null
        }
      }
    case CANCEL_MESSAGE:
      return {
        ...state,
        message: null
      }
    case REQUEST_TASKS:
      return {
        ...state,
        tasksAreLoading: true
      }
    case RECEIVE_TASKS:
      return {
        ...state,
        tasks: action.tasks,
        startTime: action.lastSync,
        lastSync: action.lastSync,
        tasksAreLoading: false
      }
    case START_UPDATE_MONITOR:
      return {
        ...state,
        monitorUpdates: true
      }
    case STOP_UPDATE_MONITOR:
      return {
        ...state,
        monitorUpdates: false
      }
    case RECEIVE_TASK_UPDATES:
      return {
        ...state,
        tasks: updateTasks(state.tasks, action.updatedTasks, action.deletedTaskIds),
        lastSync: action.lastSync,
      }
    case ADD_POMODORO:
    case REMOVE_POMODORO:
    case COMPLETE_POMODORO:
    case UNCOMPLETE_POMODORO:
    case COMPLETE_TASK:
    case COMPLETED_TASK:
    case SAVED_POMODOROS:
      return {
        ...state,
        tasks: state.tasks.map( t => action.id === t.id ? task(t, action) : t )
      }
    default:
      return state
  }
}

export { task, app, updateTasks }
