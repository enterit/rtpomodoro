import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import Main from './components/Main'
import Credits from './components/Credits'
import Authenticated from './components/Authenticated'
//import TestPage from './components/TestPage'
import Tasks from './components/Tasks'
import promiseFinally from 'promise.prototype.finally'

promiseFinally.shim()

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/">
      <IndexRoute component={Main}/>
      <Route path="tasks" component={Tasks}/>
      <Route path="credits" component={Credits}/>
      <Route path="authenticated.html" component={Authenticated}/>
    </Route>
  </Router>,
  document.getElementById('root')
)
