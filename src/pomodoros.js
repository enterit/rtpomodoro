const substringCount = (string, substring) => string.split(substring).length - 1

const pomodorosToNote = ({ completed, total }) => [].concat(
  Array(completed).fill('[*]'),
  Array(total - completed).fill('[]')
).join('')


/**
 * Creates pomodoros object using task notes.
 * Note format:
 *  [*][*][], where [*] means completed pomodoro, [] means incomplete pomodoro
 * @param noteText
 * @param taskId
 * @return {*}
 */
const pomodorosFromNote = (noteText) => {
  /**
   * At some point RTM stopped returning note title as a separate field, and started including it as the first
   * line of the note text. We support both text formats here: '[*][*]...', and 'pomodoro\n[*][*]...'.
   */
  const pomodoroRegex = /^(pomodoro\n){0,1}(\[\*{0,1}\])*$/
  if (pomodoroRegex.test(noteText)) {
    const total = substringCount(noteText, '[')
    const completed = substringCount(noteText, '*')
    return { total, completed }
  } else {
    return { total: 0, completed: 0 }
  }
}

export { pomodorosFromNote, pomodorosToNote }
