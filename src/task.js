const hasPomodoroTag = (task) => task.tags.indexOf('pomodoro') >= 0
export { hasPomodoroTag }
